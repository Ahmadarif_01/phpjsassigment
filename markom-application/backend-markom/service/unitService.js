module.exports = exports = (server, pool) => {
    server.post('/api/get_unit', (req, res) => {
        const { searchCode, searchName, searchCB, searchDT, order, page, pagesize } = req.body;
        let qFilterCode = searchCode != "" ? ` AND "code" LIKE '%${searchCode}%' ` : ``;
        let qFilterName = searchName != "" ? ` AND "name" LIKE '%${searchName}%' ` : ``;
        let qFilterCB = searchCB != "" ? ` AND "created_by" LIKE '%${searchCB}%' ` : ``;
        let qFilterDT = searchDT != "" ? ` AND "created_date" LIKE '%${searchDT}%' ` : ``;
        let qOrder = order != "" ? `ORDER BY "code" DESC` : `ORDER BY "code"`
        let perpageName = (page - 1) * pagesize;

        let query = `Select "name", "code", "created_by", TO_CHAR("created_date" :: DATE, 'yyyy-mm-dd') as tgl from "unit" where "is_delete" = 'false' ${qFilterCode} ${qFilterName} ${qFilterCB} ${qFilterDT} ${qOrder} LIMIT ${pagesize} 
        OFFSET ${perpageName}`;
        // console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/getAll', (req, res) => {

        var query = `Select "id", "name", "code", "created_by", TO_CHAR("created_date" :: DATE, 'yyyy-mm-dd') as tgl from "unit" where "is_delete" = 'false'`

        // console.log(query);
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_unitCode/:code', (req, res) => {
        const code = req.params.code;
        var query = `Select * from "unit" where "code" = '${code}'`;
        // console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    function getUnitCode(callback) {
        var query = `Select "code" From "unit"
        ORDER BY "code" DESC LIMIT 1`;
        var newCode = `UN-`;
        pool.query(query, (error, result) => {
            if (error) {
                return {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                }
            } else {
                if (result.rows.length > 0) {
                    var codeArr = result.rows[0].code.split('-');
                    newCode = newCode + ("0000" + (parseInt(codeArr[1]) + 1).toString()).slice(-4);
                    console.log(codeArr[1])
                    return callback(newCode);
                } else {
                    newCode = newCode + "0001";
                    return callback(newCode);
                }

            }

        });
    }

    server.post('/api/unit_post', (req, res) => {
        console.log(req.body);

        const {
            name,
            description
        } = req.body;

        let desc = description != "" ? `'${description}'` : null;

        getUnitCode(code => {
            var query = `INSERT INTO "unit"(
               "created_by", "created_date", "code", "name", "description", "is_delete")
               VALUES('Administator', current_timestamp, '${code}', '${name}', ${desc}, false);`

            // console.log(query)

            pool.query(query, (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(201, {
                        success: true,
                        result: "Berhasil Disimpan"
                    })
                }
            });
        })
    })

    server.put('/api/updateUnit/:code', (req, res) => {
        const code = req.params.code;

        const {
            name,
            description
        } = req.body;

        let desc = description != "" ? `'${description}'` : null;

        var query = `UPDATE "unit" SET "updated_by" = 'administator', "updated_date" = current_timestamp,
        "name" = '${name}', "description" = ${desc} WHERE "code" = '${code}';`

        // console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Update Data"
                })
            }
        });
    })

    server.put('/api/deleteUnit/:code', (req, res) => {
        const code = req.params.code;

        var query = `UPDATE "unit" SET "updated_by" = 'administator', "updated_date" = current_timestamp,
        "is_delete" = true WHERE "code" = '${code}';`

        // console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Hapus Data"
                })
            }
        });
    })

    server.post('/api/cd', (req, res) => {
        const { searchCode, searchName, searchCB, searchDT, order, page, pagesize } = req.body;
        let qFilterCode = searchCode != "" ? ` AND "code" LIKE '%${searchCode}%' ` : ``;
        let qFilterName = searchName != "" ? ` AND "name" LIKE '%${searchName}%' ` : ``;
        let qFilterCB = searchCB != "" ? ` AND "created_by" LIKE '%${searchCB}%' ` : ``;
        let qFilterDT = searchDT != "" ? ` AND "created_date" LIKE '%${searchDT}%' ` : ``;

        var query = `select count("id") as totaldata from "unit" where "is_delete" = false  ${qFilterCode}
        ${qFilterName} ${qFilterCB} ${qFilterDT}`;
        // console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })

    });

}
import React from 'react';

export default class Header extends React.Component {
    render() {
        return (
            <header class="main-header">

                <a href='/' class="logo">
                    <span class="logo-mini"><b>M</b>RM</span>
                    <span class="logo-lg"><b>Markom</b>Application</span>
                </a>

                <nav class="navbar navbar-static-top">
                    <a href="/" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                </nav>
            </header>
        )
    }
}
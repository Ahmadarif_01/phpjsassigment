import React from 'react';

export default class Sidebar extends React.Component {
    render() {
        return (
            <aside class="main-sidebar">

                <section class="sidebar">

                    <ul class="sidebar-menu" data-widget="tree">
                        <li>
                            <a href="/unit">
                                <i class="fa fa-th"></i> <span>Unit</span>
                            </a>
                        </li>

                        <li>
                            <a href="/souvenir">
                                <i class="fa fa-th"></i> <span>Souvenir</span>
                            </a>
                        </li>
                    </ul>
                </section>
            </aside>
        )
    }
}
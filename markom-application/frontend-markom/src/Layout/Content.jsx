import React from 'react';
import Sidebar from '../Layout/Sidebar';
import Header from '../Layout/Header';
import {Route, Switch } from 'react-router-dom';
import Unit from '../Unit';
import Souvenir from '../Souvenir';


function App() {
    return (
      <div className="wrapper">
        <Header />
        <Sidebar />
        <div className="content-wrapper">
  
          <section className="content">
            <Switch>
              <Route exact path="/Unit" component={Unit} />
              <Route exact path="/Souvenir" component={Souvenir} />
            </Switch>
            </section>
        </div>
      </div>
    )
  }
  
  export default App;
  
import axios from 'axios';
import { config } from '../Config/config';

export const souvenirService = {
    post: (item) => {
        const result = axios.post(config.apiUrl + '/souvenir_post', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateData: (item) => {
        const result = axios.put(config.apiUrl + '/updateSouvenir/' +item.code, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
      
        return result;
      },

      deleteSouvenir: (item) => {
        const result = axios.put(config.apiUrl + '/deleteSouvenir/' +item.code)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
      
        return result;
      },

    getData: (filter) => {
        const result = axios.post(config.apiUrl + '/get_souvenir', filter)

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        console.log(result);
        return result;
    },

    getDataUnit: () => {
        const result = axios.get(config.apiUrl + '/getAll_souvenir' )

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        console.log(result);
        return result;
    },

    getdatabycode: (code) => {
        const result = axios.get(config.apiUrl + '/get_souvenirCode/' + code)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    countData: (filter) => {
        const result = axios.post(config.apiUrl + '/cd_souvenir', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

}
export default souvenirService;


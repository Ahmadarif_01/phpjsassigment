import axios from 'axios';
import { config } from '../Config/config';

export const unitService = {
    post: (list_user) => {
        const result = axios.post(config.apiUrl + '/unit_post', list_user)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateData: (item) => {
        const result = axios.put(config.apiUrl + '/updateUnit/' +item.code, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
      
        return result;
      },

      deleteUnit: (item) => {
        const result = axios.put(config.apiUrl + '/deleteUnit/' +item.code)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
      
        return result;
      },

    getAll: (filter) => {
        const result = axios.post(config.apiUrl + '/get_unit', filter)

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // console.log(result);
        return result;
    },

    getDataUnit: () => {
        const result = axios.get(config.apiUrl + '/getAll' )

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        console.log(result);
        return result;
    },

    getdatabycode: (code) => {
        const result = axios.get(config.apiUrl + '/get_unitCode/' + code)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    countData: (filter) => {
        const result = axios.post(config.apiUrl + '/cd', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
}
export default unitService;


import React from 'react';
import Modal from 'react-bootstrap/Modal';


class FormInput extends React.Component {
    render() {
        const { open_modal, close, mode, changeHandler, errors, onSave, list_unit, selectHandler_unit, m_souvenir } = this.props;
        return (
            <div>
                <Modal show={open_modal} style={{ opacity: 1 }}>
                    <Modal.Header style={{ backgroundColor: 'lightblue' }}>
                        <Modal.Title>{mode === 'create' ? <label>Add Souvenir</label> : <label>Update Souvenir</label>}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                       
                        <form>
                            <div class="form-group">
                                <label>Souvenir Name *</label>
                                <input type="text" class="form-control" placeholder="Type Souvenir Name"
                                    onChange={changeHandler('name')} value={m_souvenir.name}/>
                                <span style={{ color: "red" }}>{errors["name"]}</span>
                            </div>
                            <div class="form-group">
                                <label>Unit Name *</label>
                                <select class="form-control" onChange={selectHandler_unit('mUnitId')}>
                                <option disabled selected>-Choose Unit-</option>
                                {
                                        list_unit.map(data => {
                                            return (
                                                <option value={data.id} selected={m_souvenir.unitId === data.id}>{data.name}</option>
                                            )
                                        })
                                    }
                                </select>
                                <span style={{ color: "red" }}>{errors["mUnitId"]}</span>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" placeholder="Type Description"
                                 onChange={changeHandler('description')} value={m_souvenir.description}/>
                            </div>
                        </form>
                    </Modal.Body>
                    {/* {JSON.stringify(m_souvenir)} */}
                            <Modal.Footer>
                                <div class='btn-group'>
                                    <button class='btn btn-primary' onClick={onSave}>Save</button>
                                    <button class='btn btn-danger' onClick={close}>Cancel</button>
                                </div>
                            </Modal.Footer>
                </Modal>
            </div>

        )
    }
}
export default FormInput;
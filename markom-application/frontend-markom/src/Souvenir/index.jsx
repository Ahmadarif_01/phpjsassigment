import React from 'react';
import Table from 'react-bootstrap/Table';
import FormInput from './formInput';
import Modal from 'react-bootstrap/Modal';
import souvenirService from '../Service/souvenirService';
import unitService from '../Service/unitService';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";


class Souvenir extends React.Component {

    m_souvenir = {
        name: '',
        description: '',
        mUnitId: ''
    }

    constructor() {
        super();
        this.state = {
            open_modal: false,
            open_delete: false,
            open_detail: false,
            m_souvenir: this.m_souvenir,
            list_souvenir: [],
            list_unit: [],
            mode: '',
            errors: {},
            filter: {
                searchCode: '',
                searchName: '',
                searchCB: '',
                searchDT: '',
                searchUnit: '',
                order: '',
                page: '1',
                pagesize: '10'
            },
            totaldata: 1,
        }
    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            m_souvenir: {
                ...this.state.m_souvenir,
                [name]: value
            }
        })

    }

    open = () => {
        this.getUnit()
        this.setState({
            open_modal: true,
            mode: 'create',
            m_souvenir: {
                name: '',
                description: '',
                mUnitId: ''
            },
            errors: {}
        })
    }

    close = () => {
        this.setState({
            open_modal: false
        })
    }

    closeDetail = () => {
        this.setState({
            open_detail: false
        })
    }

    closeDelete = () => {
        this.setState({
            open_delete: false
        })
    }

    handleValidation = () => {
        // const { m_souvenir } = this.state;

        let fields = this.state.m_souvenir;
        let errors = {};
        let formIsValid = true;

        if (!fields['name']) {
            formIsValid = false;
            errors['name'] = 'Jangan Kosong !';
        }

        if (!fields['mUnitId']) {
            formIsValid = false;
            errors['mUnitId'] = 'Jangan Kosong !';
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    loadList = async (filter) => {
        const respon = await souvenirService.getData(filter);
        const countData = await unitService.countData(filter);
        if (respon.success) {
            this.setState({
                list_souvenir: respon.result,
                totaldata: Math.ceil(countData.result[0].totaldata / filter.pagesize)
            })
        }
    }

    selectHandler_unit = name => ({ target: { value } }) => {
        this.setState({
            m_souvenir: {
                ...this.state.m_souvenir,
                [name]: value
            }
        })
    }

    componentDidMount() {
        this.loadList(this.state.filter)
    }

    componentDidMount() {
        const { filter } = this.state;
        this.loadList(filter);
        this.getUnit();
    }

    componentDidUpdate() {
        const { filter } = this.state;
        this.loadList(filter);
    }

    hendlerEdit = async (code) => {
        this.getUnit();
        const respon = await souvenirService.getdatabycode(code);
        if (respon.success) {
            this.setState({
                open_modal: true,
                mode: 'edit',
                m_souvenir: respon.result[0]
            })
        } else {
            alert(respon.result);
        }
        this.setState({
            errors: {}
        })
    }

    hendlerDel = async (code) => {
        const respon = await souvenirService.getdatabycode(code);
        if (respon.success) {
            this.setState({
                open_delete: true,
                m_souvenir: respon.result[0]
            })
        } else {
            alert(respon.result);
        }
    }

    sureDelete = async (item) => {
        const { m_souvenir } = this.state;

        const respon = await souvenirService.deleteSouvenir(m_souvenir);
        if (respon.success) {
            alert(respon.result)
            this.loadList();
        } else {
            alert(respon.result)
        }
        this.setState({
            open_delete: false
        })
    }

    onSave = async () => {
        const { m_souvenir, mode, filter } = this.state;

        if (mode === 'create') {
            if (this.handleValidation()) {
                const respon = await souvenirService.post(m_souvenir);
                if (respon.success) {
                    alert(respon.result)
                } else {
                    alert(respon.result)
                }
                this.loadList(filter)
                this.setState({
                    open_modal: false,
                })
            }
        } else {
            if (this.handleValidation()) {
                const respon = await souvenirService.updateData(m_souvenir);
                if (respon.success) {
                    alert(respon.result)
                } else {
                    alert(respon.result)
                }
                this.loadList(filter)
                this.setState({
                    open_modal: false
                })
            }
        }
    }

    getUnit = async () => {
        const respon = await unitService.getDataUnit();
        if (respon.success) {
            this.setState({
                list_unit: respon.result
            })
        }
    }

    openDetail = async (code) => {
        const respon = await souvenirService.getdatabycode(code);
        if (respon.success) {
            this.setState({
                open_detail: true,
                m_souvenir: respon.result[0]
            })
        } else {
            alert(respon.result);
        }
    }

    onChangePage = (number) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        });
    }

    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        );
    }

    pageSizeHandler = (val) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ['pagesize']: val
            }
        })
    };

    FilterchangeHandler = name => ({ target: { value } }) => {
        this.setState({
            filter: {
                ...this.state.filter,
                [name]: value
            }
        })

    }

    on_cari = () => {
        const { filter } = this.state;
        if (filter.searchCode === '' && filter.searchName === '' && filter.searchCB === '' && filter.searchDT === '') {
            alert('Data pencarian harus diisi !!!')

        }
        else {
            this.loadList(filter);
        }
    }

    reset = () => {
        const { filter } = this.state;
        this.loadList(filter);
        this.setState({
            filter: {
                searchCode: '',
                searchName: '',
                searchCB: '',
                searchDT: '',
                order: '',
                page: '1',
                pagesize: '10'
            }
        })
        document.getElementById("cariCode").value = ''
        document.getElementById("cariName").value = ''
        document.getElementById("cariCB").value = ''
        document.getElementById("cariDT").value = ''
        document.getElementById("cariUnit").value = '-Search Unit-'
    }

    handlerSorting = () => {
        let order = "";
        const { filter } = this.state;
        if (filter.order === "") {
            order = "DESC"
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["order"]: order
            }
        })
    }




    render() {
        const { open_modal, errors, mode, list_souvenir, list_unit, open_detail, m_souvenir, open_delete } = this.state;
        return (
            <div>
                <h3>List Souvenir</h3>

                <div class='btn-group pull-right'>

                    <button type='button' class='btn btn-info' onClick={this.handlerSorting}><i class="fa fa-sort-alpha-asc"></i></button>
                    <button type="button" class="btn btn-danger dropdown-toogle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-navicon"></i>
                    </button>

                    <ul class="dropdown-menu">
                        <li><a href="#" onClick={() => this.pageSizeHandler('5')}>5</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('10')}>10</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('20')}>20</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('30')}>30</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('40')}>40</a></li>
                    </ul>
                    <button type='button' class='btn btn-primary pull-right' onClick={this.open}><i class="fa fa-plus-circle"></i></button>
                </div>
                <div class='pull-right'>
                    <button class='btn btn-warning' onClick={this.on_cari}><i class="fa fa-search"></i></button>
                </div>
                {/* {JSON.stringify(this.state.filter)} */}
                <div class='form-row'>
                    <div class='col-md-2 mb-3'>
                        <input type='text' placeholder='-Search Code Souvenir-' id='cariCode' class="form-control" onChange={this.FilterchangeHandler('searchCode')} />
                    </div>
                    <div class='col-md-2 mb-3'>
                        <input type='text' placeholder='-Search Name Souvenir-' id='cariName' class="form-control" onChange={this.FilterchangeHandler('searchName')} />
                    </div>
                    <div class="col-md-2 mb-3">
                        <select class="form-control" id='cariUnit' onChange={this.FilterchangeHandler('searchUnit')}>
                            <option disabled selected>-Search Unit-</option>
                            {
                                list_unit.map(data => {
                                    return (
                                        <option value={data.name}>{data.name}</option>
                                    )
                                })
                            }
                        </select>
                    </div>
                    <div class='col-md-2 mb-1'>
                        <input type='date' id='cariDT' class="form-control" onChange={this.FilterchangeHandler('searchDT')} />
                    </div>
                    <div class='col-md-2 mb-3'>
                        <input type='text' placeholder='-Search Created-' id='cariCB' class="form-control" onChange={this.FilterchangeHandler('searchCB')} />
                    </div>
                </div>
                <br />



                <FormInput open_modal={open_modal} open={this.open} close={this.close} mode={mode}
                    changeHandler={this.changeHandler} errors={errors}
                    m_souvenir={m_souvenir} onSave={this.onSave} list_unit={list_unit}
                    selectHandler_unit={this.selectHandler_unit} />

                <Modal show={open_detail} style={{ opacity: 1 }}>
                    <Modal.Header style={{ background: 'lightgrey' }}>
                        <Modal.Title>View Unit - {m_souvenir.name} ({m_souvenir.code})</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {/* {JSON.stringify(m_souvenir)} */}
                        <from>
                            <div>
                                <label>Code : {m_souvenir.code}</label>
                                <br />
                                <label>Souvenir Name : {m_souvenir.name}</label>
                                <br />
                                <label>Souvenir Name : {m_souvenir.unitName}</label>
                                <br />
                                <label>Description : {m_souvenir.description}</label>
                            </div>
                        </from>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="btn-group">
                            <button className='btn btn-danger' onClick={this.closeDetail}>Close</button>
                        </div>
                    </Modal.Footer>
                </Modal>

                <Modal show={open_delete} style={{ opacity: 1 }}>
                    <Modal.Body>
                        Apakah anda akan menghapus data ini {m_souvenir.code} ?
                        </Modal.Body>
                    <Modal.Footer>
                        <div class='btn-group'>
                            <button class='btn btn-success' onClick={() => this.sureDelete(m_souvenir.code)}>Ya</button>
                            <button class='btn btn-info' onClick={this.closeDelete}>Tidak</button>
                        </div>
                    </Modal.Footer>
                </Modal>

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Souvenir Code</th>
                            <th>Souvenir Name</th>
                            <th>Unit</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            list_souvenir.map(data => {
                                return (
                                    <tr>
                                        <td>{data.code}</td>
                                        <td>{data.name}</td>
                                        <td>{data.unitName}</td>
                                        <td>{data.created_by}</td>
                                        <td>{data.tgl}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-success" onClick={() => this.openDetail(data.code)}><i class="fa fa-search"></i></button>
                                                <button class="btn btn-primary" onClick={() => this.hendlerEdit(data.code)}><i class="fa fa-pencil"></i></button>
                                                <button class="btn btn-danger" onClick={() => this.hendlerDel(data.code)}><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })

                        }

                    </tbody>
                </Table>
                <div class="pull-right">
                    {this.renderPagination()}
                </div>
            </div>
        )
    }
}

export default Souvenir;
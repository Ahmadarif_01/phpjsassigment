--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

-- Started on 2020-08-10 08:18:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 207 (class 1259 OID 24593)
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company (
    id bigint NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    address character varying(255),
    phone character varying(50),
    email character varying(50),
    is_delete boolean NOT NULL,
    created_by character varying(50) NOT NULL,
    created_date date NOT NULL,
    updated_by character varying,
    updated_date date
);


ALTER TABLE public.company OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24591)
-- Name: m_company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.company ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.m_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 2827 (class 0 OID 24593)
-- Dependencies: 207
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company (id, code, name, address, phone, email, is_delete, created_by, created_date, updated_by, updated_date) FROM stdin;
2	CP0001	PT.Xsis Mitra utama	\N	\N	\N	f	Administator	2020-08-09	\N	\N
3	CP0002	Equine Global	\N	\N	\N	f	Administator	2020-08-09	\N	\N
4	CP0003	Niaga Prima Paramita	\N	\N	\N	f	Administator	2020-08-09	\N	\N
\.


--
-- TOC entry 2833 (class 0 OID 0)
-- Dependencies: 206
-- Name: m_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_company_id_seq', 4, true);


--
-- TOC entry 2699 (class 2606 OID 24600)
-- Name: company m_company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT m_company_pkey PRIMARY KEY (id);


-- Completed on 2020-08-10 08:19:10

--
-- PostgreSQL database dump complete
--


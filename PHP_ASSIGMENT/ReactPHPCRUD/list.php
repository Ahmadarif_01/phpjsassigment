<?php

require 'connect.php';
error_reporting(E_ERROR);
$users = [];
$sql = "SELECT * FROM user";

if($result = mysqli_query($con,$sql)){
    $cr = 0;
    while($row = mysqli_fetch_assoc($result)){
        $users[$cr]['id'] = $row['id'];
        $users[$cr]['first_name'] = $row['first_name'];
        $users[$cr]['last_name'] = $row['last_name'];
        $cr++;
    }

    echo json_encode($users);
}else{
    http_response_code(404);
}

?>
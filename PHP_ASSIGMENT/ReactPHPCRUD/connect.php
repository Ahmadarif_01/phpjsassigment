<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS' , '');
define('DB_NAME', 'db_user');

function connect(){
    $connect = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if(mysqli_connect_error($connect)){
        die('Failed to Connect: ' . mysqli_connect_error());
    }

    mysqli_set_charset($connect, 'utf8');

    return $connect;
}

    $con = connect();
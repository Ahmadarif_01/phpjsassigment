import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import View from './components/View';
import Insert from './components/Insert';
import Edit from './components/Edit';

function App() {
  return (
    <Router>
      <div className='container'>
        <nav className='navbar navbar-expand-lg navbar-light bg-light'>
          <Link to={'/'} className='nav-link'>React CRUD</Link>
          <ul className='navbar-nav mr-auto'>
            <li className='nav-item'>
              <Link to={'/'} className='nav-link'>Home</Link>
            </li>
             <li className='nav-item'>
              <Link to={'/insert'} className='nav-link'>Insert</Link>
            </li>
             <li className='nav-item'>
              <Link to={'/view'} className='nav-link'>View</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route exact path='/insert' component={ Insert } />
          <Route exact path='/edit/:id' component={ Edit } />
          <Route exact path='/view' component={ View } />
        </Switch>
      </div>
    </Router>
  );
}

export default App;

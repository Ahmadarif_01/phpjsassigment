import React, { Component } from 'react';
import axios from 'axios';

export default class Insert extends Component {

    constructor(props) {
        super(props);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            first_name: '',
            last_name: ''
        }
    }

    onChangeLastName(e) {
        this.setState({
            last_name: e.target.value
        });
    }

    onChangeFirstName(e) {
        this.setState({
            first_name: e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();

        const obj = {
            first_name: this.state.first_name,
            last_name: this.state.last_name
        };

        var config = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
        };

        axios.post('http://localhost/ReactPHPCRUD/insert.php', obj, config)
        .then(res => console.log(res.data));
        // console.log(obj)

        this.setState({
            first_name: '',
            last_name :''
        })
    }

    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3>Add New User</h3>
                <form onSubmit={this.onSubmit}>
                    <div className='form-group'>
                        <label>First Name: </label>
                        <input type='text' className='form-control' onChange={this.onChangeFirstName} value={this.state.first_name} />
                    </div>
                    <div className='form-group'>
                        <label>Last Name: </label>
                        <input type='text' className='form-control' onChange={this.onChangeLastName} value={this.state.last_name} />
                    </div>
                    <div className='form-group'>
                        <input type='submit' value='Register User' className='btn btn-primary' />
                    </div>
                </form>
            </div>
        )
    }
}
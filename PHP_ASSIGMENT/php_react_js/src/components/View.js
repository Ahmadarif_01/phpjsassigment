import React, { Component } from 'react';
import axios from 'axios';
import RecordList from './RecordList';

export default class View extends Component {

    constructor(props) {
        super(props);
        this.state = { users: [] };
    }

    componentDidMount() {
        axios.get('http://localhost/ReactPHPCRUD/list.php')
            .then(response => {
                this.setState({ users: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    userList() {
        return this.state.users.map(function (object, i) {
            return <RecordList obj={object} key={i} />;
        })
    }

    render() {
        return (
            <div>
                <h3 align='center'>User List</h3>
                <table className='table table-striped' style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th colSpan='2'>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.userList()}
                    </tbody>
                </table>
            </div>
        )
    }
}
import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router'

export default class Edit extends Component {

    constructor(props) {
        super(props);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            first_name: '',
            last_name: '',
            redirect: false
        }
    }

    onChangeLastName(e) {
        this.setState({
            last_name: e.target.value
        });
    }

    onChangeFirstName(e) {
        this.setState({
            first_name: e.target.value
        });
    }

    componentDidMount() {
        axios.get('http://localhost/ReactPHPCRUD/getById.php?id=' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    first_name: response.data.first_name,
                    last_name: response.data.last_name
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onSubmit(e) {
        e.preventDefault();

        const obj = {
            first_name: this.state.first_name,
            last_name: this.state.last_name
        };

        var config = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        };

        axios.put('http://localhost/ReactPHPCRUD/update.php?id='+this.props.match.params.id, obj, config)
            .then(res => {
                if (res.status) {
                    this.setState({ redirect: true })
                }
            })

        // console.log(obj)
    }

    render() {

        const { redirect } = this.state;

        if (redirect) {
            return <Redirect to='/view' />;
        }
        return (
            <div style={{ marginTop: 10 }}>
                <h3>Add New User</h3>
                <form onSubmit={this.onSubmit}>
                    <div className='form-group'>
                        <label>First Name: </label>
                        <input type='text' className='form-control' onChange={this.onChangeFirstName} value={this.state.first_name} />
                    </div>
                    <div className='form-group'>
                        <label>Last Name: </label>
                        <input type='text' className='form-control' onChange={this.onChangeLastName} value={this.state.last_name} />
                    </div>
                    <div className='form-group'>
                        <input type='submit' value='Register User' className='btn btn-primary' />
                    </div>
                </form>
            </div>
        )
    }
}